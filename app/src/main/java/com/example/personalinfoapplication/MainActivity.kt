package com.example.personalinfoapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    private lateinit var buttonDone: Button
    private lateinit var textFirstName: EditText
    private lateinit var textSecondName: EditText
    private lateinit var textLastName: EditText
    private lateinit var textAge: EditText
    private lateinit var textHobby: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textFirstName = findViewById(R.id.editTextFirstName)
        textSecondName = findViewById(R.id.editTextSecondName)
        textLastName = findViewById(R.id.editTextLastName)
        textAge = findViewById(R.id.editTextAge)
        textHobby = findViewById(R.id.editTextHobby)
        buttonDone = findViewById(R.id.buttonDone)
        buttonDone.setOnClickListener {
            val person = PersonInfo(textFirstName.text.toString(),
                textSecondName.text.toString(),
                textLastName.text.toString(),
                textAge.text.toString().toInt(),
                textHobby.text.toString())
            InfoActivity.newIntent(this@MainActivity, person).also {
                startActivity(it)
            }
        }
    }
}